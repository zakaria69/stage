

export default function Bon(){
    return(
        <>
            <div className="bon-form container"> 
                <div class="mb-3">
                    <div class="form-floating">
                        <select class="form-select" id="floatingSelect" aria-label="Floating label select example">
                            <option selected>Produits...</option>
                            <option value="1">Produit 1</option>
                            <option value="2">Produit 2</option>
                            <option value="3">Produit 3</option>
                        </select>
                    <label for="floatingSelect">Choose your product</label>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="Quontite" class="form-label"></label>
                    <input type="number" class="form-control" id="Quontite" placeholder="Quotité"/>
                </div>
            
                    <div className="btn-valid">
                        <input type="submit" class="btn btn-danger" value={"Valider"}/>
                    </div>
            </div>

            <div className="bon-table container">
            <table class="table">
  <thead>
    <tr>
      <th scope="col">Ref</th>
      <th scope="col">Produits</th>
      <th scope="col">Quontité</th>
      <th scope="col">PRHT</th>
      <th scope="col">PRTTC</th>
      <th scope="col">Reste</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Produit 1</td>
      <td>15</td>
      <td>90</td>
      <td>115</td>
      <td>5</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Produit 2</td>
      <td>20</td>
      <td>55</td>
      <td>96</td>
      <td>26</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Produit 3</td>
      <td>6</td>
      <td>65</td>
      <td>105</td>
      <td>75</td>
    </tr>
  </tbody>
</table>
<div className="btn-valid">
                        <input type="submit" class="btn btn-danger" value={"Enregistrer"}/>
                    </div>
            </div>
        </>
    );
}