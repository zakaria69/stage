import React from 'react';
import { useState } from "react";
import DashBoard from './DashBoard';
import Header from './Navbar';
import Products from './products';
import Bon from './Bon';



export default function Connected(){
    
    
    const data = [
        {
          img:"OIP.jpg",
          titre:"Product 1",
          quontite:30,
        },
        {
          img:"OIP.jpg",
          titre:"Product 2",
          quontite:50,
        },
        {
          img:"OIP.jpg",
          titre:"Product 3",
          quontite:10,
        },
        {
          img:"OIP.jpg",
          titre:"Product 4",
          quontite:70,
        },
        {
          img:"OIP.jpg",
          titre:"Product 5",
          quontite:15,
        },
        {
          img:"OIP.jpg",
          titre:"Product 6",
          quontite:60,
        },
        {
          img:"OIP.jpg",
          titre:"Product 7",
          quontite:90,
        },
        {
          img:"OIP.jpg",
          titre:"Product 8",
          quontite:30,
        },
    ];
    const [nouveau , setNouveau] = useState(false);
    const [stock , setStock] = useState(true);

    const handleNouveau = () =>{
        if(nouveau === false){
            setNouveau(true)
            setStock(false)
        }
    }
    const handleStock = () =>{
        if(stock === false){
            setNouveau(false)
            setStock(true)
        }
    }
    return(
        <>
            <Header button='Mr.SDDS'/>
            <div className='main-section'>
                <div>
                    <DashBoard nouveau={handleNouveau} stock={handleStock}/>
                </div>
                <div className="Bon">
                {stock && <Products datalist={data}/>}
                {nouveau && <Bon/>}
                </div>       
            </div>
        </>
    );
}