import React from 'react';
import Form from "./InscForm";
import Header from "./Navbar";


export default function Home(){
    return (
      <>
        <Header button='Se Connect'/>
        <Form/>
      </>  
    );
}