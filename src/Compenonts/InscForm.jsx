import { Link } from "react-router-dom";
import Connected from "./Connected";



export default function Form(){

    return (
        <>
        <form className="container form-insc" action="" >
            <div className="form-head">
                <h1>LMM</h1>
            </div>
            <div className="mb-3">
                <label for="exampleFormControlInput1" className="form-label">Email address</label>
                <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Entrez votre email"/>
            </div>
            <div className="mb-3">
                <label for="exampleFormControlInput1" className="form-label">Mot de pass</label>
                <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Mot de pass"/>
            </div>
            <div className="sub-button">
                <Link to={"/Connected"} element={<Connected/>}><input class="btn btn-danger" type="submit" value="Connectez-vous"/></Link>
            </div>
        </form>
        </>
    );
}