



export default function Product(props){
    return(
        <>
            <div className="prod">
                <div className="prod-img">
                    <img src={`./img/${props.img}`} alt="produit" />
                </div>
                <div className="desc">
                    <h4>{props.titre}</h4>
                    <p>Quontité : {props.quontite} </p>
                </div>
            </div>
        </>
    );
}