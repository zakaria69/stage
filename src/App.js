import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Home from './Compenonts/Home';
import Connected from './Compenonts/Connected';


function App() {
  return (
 <>
    <BrowserRouter>
      <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/Connected' element={<Connected/>}/>
      </Routes>
    </BrowserRouter>
 </>
  );
}

export default App;
/* */